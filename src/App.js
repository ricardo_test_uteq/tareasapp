import './App.css';
import React from 'react';

import { Boton } from './Boton';
import { Buscador } from './Buscador';
import { Contador } from './Contador';
import { Item } from './Item';
import { Lista } from './Lista';

const tareasDefault = [
  { text: 'Jugar fútbol', completed: false},
  { text: 'Leer un nuevo libro', completed: false},
  { text: 'Dibujar un mapa', completed: false},
  { text: 'Comer más frutas y verduras', completed: false },
];
function App() {


   //------Persistencia de datos---------
  const localStorageTareas = localStorage.getItem('TAREAS_V1');
  let parsedTareas;

  if (!localStorageTareas) {
    localStorage.setItem('TAREAS_V1', JSON.stringify([]));
  } else {
    parsedTareas = JSON.parse(localStorageTareas);
  }

   //-------guardar tareas en el estado-------
   const guardarTareas = (newTarea) => {
    const stringTareas = JSON.stringify(newTarea);
    localStorage.setItem('TAREAS_V1', stringTareas);
    setTareas(newTarea);
  };

//CONTADOR

    //Estado
    const [tareas, setTareas] = React.useState(parsedTareas)
    const completedTareas = tareas.filter(tarea=> !! tarea.completed).length
    // Variable que almacena el total de tareas
    const totalTareas = tareas.length


const completarTarea = (text)=>{
  const tareaIndex = tareas.findIndex(tarea => tarea.text == text)
  const neuevasTareas = [...tareas]
  neuevasTareas[tareaIndex].completed =true
  guardarTareas(neuevasTareas)

  }

const eliminarTarea = (text)=>{
  const tareaIndex = tareas.findIndex(tarea => tarea.text == text)
  const neuevasTareas = [...tareas]
  neuevasTareas.splice(tareaIndex, 1)
  guardarTareas(neuevasTareas)

  }
  
  // BUSCADOR
  //Crear el estado
  const [buscandoValor, setBuscandoValor] = React.useState('');
  //Crear un array vacio
  let buscarTareas = [];

  //Condicional
  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tarea) => {
      const tareaText = tarea.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }
  return (
    <div>
       <Contador total={totalTareas} completed={completedTareas} />
       
      <Buscador
        buscandoValor={buscandoValor}
        setBuscandoValor={setBuscandoValor}
      />
       <Lista>

      
{buscarTareas.map(tarea =>( 
   <Item 
   text={tarea.text}
   key={tarea.text}
   completed = {tarea.completed} 
   onComplete={()=> completarTarea(tarea.text)}
   onDelete={()=> eliminarTarea(tarea.text)}
   />
  
 ))}

</Lista>
      <Boton />
    </div>
  );
}

export default App;
