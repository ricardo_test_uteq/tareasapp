import React from 'react';
import '../App.css';
import './Buscador.css';

function Buscador({ buscandoValor, setBuscandoValor }) {
  // estado
  const buscando = (event) => {
    console.log(event.target.value);
    setBuscandoValor(event.target.value);
  };
  return (
    <input
      placeholder='Buscar una tarea.......'
      value={buscandoValor}
      onChange={buscando}
    />
  );
}

export { Buscador };

