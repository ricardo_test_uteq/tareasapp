import '../App.css';
import './Item.css';

import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import CheckIcon from '@mui/icons-material/Check';
import { Box } from '@mui/material';


function Item(props) {
  return (
    <li>
      <span
        onClick={props.onComplete}
      >
        <CheckIcon />
      </span>
      <p>{props.text}</p>
      <span
        onClick={props.onDelete}
      >
        <IconButton edge='end' aria-label='delete'>
          <DeleteIcon />
        </IconButton>
      
      <Box></Box>
    </span>
    </li>
  );
}

export { Item };
