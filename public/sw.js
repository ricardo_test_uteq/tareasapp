//Imports
importScripts('js/sw-utils.js');
//Crear constantes para almacenar el cahe
const ESTATICO_CACHE = 'static-v1';
const DINAMICO_CACHE = 'dinamico-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

const APP_SHELL = [
  '/',
  'index.html',
  'js/app.js',
  'js/sw-utils.js',
];

const APP_SHELL_INMUTABLE = [
  //'https://fonts.googleapis.com/css2?'
];
//Proceso de instalacion
self.addEventListener('install', (event) => {
  const cacheStatic = caches
    .open(ESTATICO_CACHE)
    .then((cache) => cache.addAll(APP_SHELL));
  const cacheInmutable = caches
    .open(INMUTABLE_CACHE)
    .then((cache) => cache.addAll(APP_SHELL_INMUTABLE));

  event.waitUntil(Promise.all[(cacheStatic, cacheInmutable)]);
});

//Proceso de activacion
self.addEventListener('activate', (event) => {
  //Eliminar cache del sw anterior
  const respuesta = caches.keys().then((keys) => {
    keys.forEach((key) => {
      if (key !== ESTATICO_CACHE && key.includes('static')) {
        return caches.delete(key);
      }
    });
  });

  event.waitUntil(respuesta);
});

//Estrategia de cache
self.addEventListener('fetch', (event) => {
  const respuesta = caches.match(event.request).then((res) => {
    if (res) {
      return res;
    } else {
      return fetch(event.request).then((newRes) => {
        //primero ir al cache sw-utils.js
        return actualizaCacheDinamico(DINAMICO_CACHE, event.request, newRes);
      });
    }
  });

  event.respondWith(respuesta);
});